

function search(){
	var keywords = $("input").val().trim();
	var result_data = [];

	if (keywords) {
		$.each(answerSourceData, function(k, item){
			if(item.title.indexOf(keywords) != -1){
				result_data.push(item);
			}
		});
	}

	return result_data;
}

function showResultData(result_data){
	var item_html = '';
	if (result_data.length) {
		// 存在结果
		$.each(result_data, function(k, item){
			item_html += '<div class="answer-title">';
			item_html += item.title;
			item_html += '</div>';
			item_html += '<div class="answer-body">';
			item_html += item.value;
			item_html += '</div>';
		});
	}else{
		// 不存在结果
		item_html += '<div class="notfound">未找到</div>';
	}
	$(".show-answer").html(item_html);
}

// 执行按钮
$(".search").click(function(e){
	runSearch();
});

// 清除
$(".clear").click(function(e){
	$("input").val("");
	$(".show-answer").html('<div class="showtips">这里展示答案</div>');
});

// 监控回车
$("input").keyup(function(){
	// 按下回车按钮时执行
	if(window.event.keyCode == 13){
		runSearch();
	}
})

// 运行检索
function runSearch(){
	var result_data = [];
	result_data = search();
	
	showResultData(result_data);
}

$(".last-update-time").html("最后更新时间："+lastUpdateTime);

